# Debugging Tips

Make sure your mutt is built with debugging support enabled and run

`mutt -v`

and look for

`+DEBUG`

If it isn't, you'll have to rebuild mutt with debugging
support:

## Build-enable debugging in mutt

* pass the `--enable-debug` flag to `configure` along with any other flags you need.  
* it can also be useful to run `make` as `make CFLAGS=-ggdb` in order to get better debugging symbols

## Generating a trace file

To produce a debug trace, run mutt with the `-d2` flag. `-d` takes a
number from 1-5, corresponding to increasing levels of
verbosity:

1. errors  
2. network tracing, important state changes  
3. less important information  
4. log spam, e.g. status updates from functions that are called very often  
5. everything, even very confidential information like passwords

For most purposes, level 2 provides the right amount of detail for a bug
report.[br](br "wikilink") It may also include some confidential
information (email addresses, server URLs), so you should read it over
before you send it to anyone.[br](br "wikilink") Feel free to sanitize
it -- we'll ask for further information if necessary.

## Analyzing a crash

We'd love to see a debugger back trace.

### If you have a core file

If mutt has produced a core file, run `gdb /path/to/mutt <core file>`,
then enter `backtrace` at the `(gdb)` prompt and cut and paste the
output.

### If you don't

One common reason for not getting a core dump when mutt crashes is
`ulimit`. [br](br "wikilink") If running `ulimit -c` returns `0`,
that means core dumps are disabled in your login session. Try executing
`ulimit -c unlimited`, then running mutt again.

If you still don't have a core dump, you can always run mutt inside of
gdb, then generate the backtrace when it crashes:

1. `gdb /path/to/mutt`   
2. `(gdb) run -d2 [other arguments you use]`   
3. (make mutt crash)  
4. `backtrace` 
